console.log("Future full-stack dev")

// Array methods

// Javascript has built-in function and methods for arrays. This allows us to manipulate and access array elements.

// [Section] Mutator Methods 
	// Mutator methods are functions that mutate or change the an array after they're created
	// These methods manipulate the original array performing various tasks as adding and removing elements 
let fruits = ["Apple", "Orange", "Kiwi", "Dragon fruit"]; 
console.log(fruits);

		// push()
		/*
			-Adds an element/s in the end of an array and returns the array's length. 
			- Syntax:
			arrayName.push(elementsToBeAdded);
		*/
	console.log("Current Array fruits[]:");
	console.log(fruits);


	let fruitsLength = fruits.push("Mango");
	console.log("Mutated array from push method: ");
	console.log(fruits);
	console.log(fruitsLength);

	//  Returns the present length/ number
	// Adding multiple elements to an array
	fruitsLength = fruits.push("Avocado", "Guava");
	console.log("Mutated array from push method: ");
	console.log(fruits);
	console.log(fruitsLength);

	// pop() 
		/*
			-Removes the last element in an array and returns the removed element 
			Syntax:
			arrayName.pop();
		*/
	console.log("Current Array fruits[]; ");
	console.log(fruits);

	let removedFruit = fruits.pop();
	console.log("Mutated Array after the pop method: ");
	console.log(fruits);
	console.log(removedFruit)

	console.log("Current Array fruits[]: ");
	removedFruit = fruits.pop(fruits);
	console.log(fruits);
	console.log(removedFruit)

	// unshift()
		/*
			-adds one ore more elements at the beginning of an array and returns the present length.
			-Syntax:
				arrayName.unshift("elementA");
				arrayName.unshift("elementA", "elementB"...)
		*/

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	fruitsLength = fruits.unshift("Lime", "Banana");
	console.log("Mutated Array after the unshift() methods: ");
	console.log(fruits);
	console.log(fruitsLength)

	// shift()

	/*
		-removes an element at the beginning of an array AND it returns the removed element.
		-Syntax:
			arrayName.shift()
	*/
	console.log("Current Array fruits[]: ");
	console.log(fruits);

	// the element that was removed was contain in "removedFruit"
	removedFruit = fruits.shift()
	console.log("Mutated Array after the unshift() methods: ");
	console.log(fruits);
	console.log(removedFruit);

	// splice()
		// Simulateneously  removes elements from a specified index number and adds elements. 
		/* Syntax:
			arrayName.splice(startingIndex,deleteCount,elementsToBeAdded)
		*/

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	fruits.splice(1, 1, "Lime");
	console.log("Mutated Array after the splice() methods: ");
	console.log(fruits);

	fruits.splice(5, 0, "Cherry");
	console.log("Mutated Array after the splice() methods: ");
	console.log(fruits);

	// Remove only on a specific index
	let index = 6
	console.log(fruits);
	fruits.splice(6, 1,);
	console.log("Mutated Array after the splice() methods: ");
	console.log(fruits);

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	fruits.splice(3, 0, "Durian", "Santol");
	console.log("Mutated Array after the splice() methods: ");
	console.log(fruits);

	// sort()
		/*
			-Rearranges the array elements in alphanumeric order
			- syntax:
			arrayName.sort()
		*/
	console.log("Current Array fruits[]: ");
	console.log(fruits);
	console.log(fruits.sort()); 

	console.log("Mutated Array after the sort() methods: ");
	console.log(fruits);

	/*
		Important Note 
		The sort method is used for more complicated functions
		Focus the bootcampers on the basic usafe of the sort method.
	*/

	// reverse()
		/*
			-reverses the order of array elements 
			syntax
				arrayName.reverse()
		*/

	console.log("Current Array fruits[]: ");
	console.log(fruits);
	console.log("Return of reverse()method: ")
	console.log(fruits.reverse());

	console.log("Mutated Array after the sort() methods: ");
	console.log(fruits);

	// [Section] Non-mutator Methods
	/*
		-Non-mutator methods are functions that do not modify or change an array after they're created
		-These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
	*/

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE", "PH"]; 

		// indexOf()
		/*
			-it returns the index number of the first matching element founf in an array 
			- if no match was found, the result will be -1 
			-the search process will be done from first element proceeding to the last element.
			Syntax: arrayName.indexOf(searchValue);	
					arrayName.indexOf(searchValue, startingIndex);	
		*/

	// Will only return the first match 
	console.log(countries.indexOf("PH")); 
	console.log(countries.indexOf("BR"));

	/*arrayName.indexOf(searchValue, startingIndex);*/
	// It return 5 because it finds in the second index
	console.log(countries.indexOf("PH", 2));

	// lastIndexOf()
	/*
		-returns the index number of the last matching element found in an array
		-the search process will be done from last element proceeding to the first element. 
		syntax:
			arrayName.lastIndexOf(searchValue)
			arrayName.lastIndexOf(searchValue, staratingIndex);
	*/
	console.log(countries.lastIndexOf("PH"));

	// Slice()
	/*
		-portion/slices from an array and returns a new array 
		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
	*/

	// Slicing off elements from a specified index to the last element

	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA); 
	console.log(countries);

	// Slicing off element from a specified index to another index. 
	// Subtract the value to see the exact elements that should be added
	let slicedArrayB = countries.slice(1,5);
	console.log(slicedArrayB);

	// Slicing off elements starting from the last element of an array

	let slicedArrayC = countries.slice(-3,-1);
	console.log(slicedArrayC);

	// toString()
	/*
		-returns an array as string separated by commas
		-syntax
		arrayName.toString(). 
	*/

	let stringedArray = countries.toString()
	console.log(stringedArray); 
	console.log(countries.toString())

	// concat()
		/*
			combines arrays and returns the combined result
			-syntax:
			arrayA.concat(arrayB);
			arrayA.concat(elementA);
		*/
	let tasksArrayA = ["drink HTML", "eat javascript"];
	let tasksArrayB = ["inhale CSS", "breathe SASS"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log("result from concat method: ");
	console.log(tasks);

	// combining multiple arrays
	console.log("Result from concat method: "); 
	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks); 

	// Combine arrays with elements
	let combinedTasks = tasksArrayA.concat("smell express", "throw react");
	console.log("Result from concat method: ");
	console.log(combinedTasks);

	// join()

	/*
		-returns an array as a string separated by specified separator string
		- syntax: 
		arrayName.join("separatorString");

	*/

	let users = ["John","Jane", "Joe", "Robert"];

	console.log(users.join());
	console.log(users.join(" "));
	console.log(users.join(" - "));

	// [Section] Iteration Methods

		/*
			Iteration methods are loop designed to perform a repetitive tasks on arrays
			Iteration methods loops over all elements in array
		*/
	// forEach()
	/*
		-similar to for loop that iterates on each of array element
		- for each element in the array, the fnction in the foreach method will be run.
		syntax:
		arrayName.forEach(function(indivElement){
			statement/s;
		}
	*/

	console.log(allTasks);

	allTasks.forEach(function(task)
	{console.log(task)
	})

	let filteredTasks = [];

	allTasks.forEach(function(task){

		if(task.length > 10){
			filteredTasks.push(task);
		}
	})

	console.log(filteredTasks)

	// map()
	/*
		-iterates on each element and returns new array with different values depending on the result of the function's operation.
		-Syntax:
			let/const resultArray = arrayName.map(function(elements){
			statements;
			return;
			})
	*/

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		return number*number;
	})

	console.log("Original Array: ");
	console.log(numbers);
	console.log("result of map method: ");
	console.log(numberMap);

	// every()
	/*
		checks if all elements in an array meet the given condition
		-this is useful validating data stored in arrays. especially when dealing with large amounts of data.
		-returns a true value if all elements meet the condition and false if otherwise. 
		syntax
		let/const resultArray = arrayName.every(functio(element){
			return expression/ condtion;
		}) 
	*/

	console.log(numbers);
	let allValid = numbers.every(function(number){
		return(number < 6);
	})

	console.log(allValid);

	// some()
	/*
		- checks if at least one element in the array meets the given condition 
		- returns a true valuf if at least one element meets the condition and false if none.
		- syntax: 
			let/const resultArray = arrayName.some(function(elements){
				return expression/ condition
			})
	*/

	console.log(numbers); 
	let someValid = numbers.some(function(number){
		return(number < 2);
	})
	console.log(someValid);

	// filter method()
	/*
		-return new array that contains the elements which meeths the given condition 
		- return an empty array if no element/s were found.
		syntax:
		let/const resultArray = arrayName.filter(function(element){
		return expression/condition;
		})
	*/

	console.log(numbers);
	let filtervalid = numbers.filter(function(number){
		return (number === 0);
	})
	console.log(filtervalid);

	// includes()
	/*
		-includes() checks if the argument passed can be found in the array.
		-it returns boolean which can be save in variable
			-returns true if the argument is found in the array
			-returns falsy if it is not.
			-case sensitive
		syntax:
			arrayName.includes(argument);
	*/

	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let productFound1 = products.includes("Mouse");
	console.log(productFound1);

	let productFound2 = products.includes("Headset");
	console.log(productFound2);

	// reduce();
	/*
		-evaluate elements from left to right and returnss/reduces the array into a single value
		syntax:
		let/const resultValue = arrayName.(reduce(function(accumulator, currentValue))
		{
			return expression/operation	
		});
	*/

	console.log(numbers);
	let total = numbers.reduce(function(x,y){
		console.log("This is the value of x: " + x);
		console.log("This is the value of y: " + y);
		return x + y;
	})	

		console.log(total);